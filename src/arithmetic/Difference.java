package arithmetic;

import java.util.Arrays;

/**
 * @title: Difference
 * @Date: 2022/2/26 22:09
 * @Version 1.0
 */
public class Difference {
    private final int[] diff;

    public Difference(int[] nums) {
        assert nums.length > 0;
        diff = new int[nums.length];
        diff[0] = nums[0];
        for (int i = 1; i < nums.length; i++) {
            diff[i] = nums[i] - nums[i - 1];
        }
    }

    public void increment(int i, int j, int val) {
        diff[i] += val;
        if (j + 1 < diff.length) {
            diff[j + 1] -= val;
        }
    }

    public int[] result() {
        int[] res = new int[diff.length];
        res[0] = diff[0];
        for (int i = 1; i < diff.length; i++) {
            res[i] = res[i - 1] + diff[i];
        }
        return res;
    }

    @Override
    public String toString() {
        return "Difference{" +
                "diff=" + Arrays.toString(diff) +
                '}';
    }

    public static void main(String[] args) {
        int[] test = new int[4];
        int[] option = new int[]{1, 2, 3};
        Difference difference = new Difference(test);
        difference.increment(option[0], option[1], option[2]);
        difference.increment(test[0], test[1], test[2]);
        System.out.println(Arrays.toString(test));
        System.out.println(difference.toString());
        System.out.println(Arrays.toString(difference.result()));
    }


}
