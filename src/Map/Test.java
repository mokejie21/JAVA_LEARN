package Map;

import java.util.*;

public class Test {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(345, "唐伯虎");
        map.put(346, "如花");

        Set<Integer> keys = map.keySet();
        for (Integer k : keys) {
            System.out.println(map.get(k));
        }

        Collection<String> values = map.values();
        for (String v : values) {
            System.out.println("v =" + v);
        }

        Set<Map.Entry<Integer, String>> entrys = map.entrySet();
        for (Map.Entry<Integer, String> entry : entrys) {
            Integer k = entry.getKey();
            String v = entry.getValue();
            System.out.println(k + v);
        }


    }
}
