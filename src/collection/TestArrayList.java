package collection;

import java.util.*;

public class TestArrayList {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, "jack", "amy", "jan");
        System.out.println(arrayList);

        for (String s : arrayList) {
            System.out.println(s);
        }
        for (int i = 0; i < arrayList.size(); i++) {
            String str = arrayList.get(i);
            System.out.println(str);
        }
        Iterator<String> str = arrayList.iterator();
        System.out.println("----------------------");
        while (str.hasNext()) {
            String data = str.next();
            System.out.println(data);
        }
        System.out.println("---------------------------");
        ListIterator<String> stringListIterator = arrayList.listIterator();
        while (stringListIterator.hasNext()) {
            String data = stringListIterator.next();
            System.out.println(data);
        }
        System.out.println("---------------------------------");
        while (stringListIterator.hasPrevious()) {
            String data = stringListIterator.previous();
            System.out.println(data);
        }

    }
}
