package collection;

import java.util.*;

public class TestLinkedList {
    public static void main(String[] args) {
        Collection<Integer> c = new LinkedList();
        c.add(100);
        c.add(200);
        c.add(300);
        System.out.println(c);

        System.out.println(c.size());
        c.remove(100);
        System.out.println(c);

        Iterator<Integer> it = c.iterator();//继承 AbstractSequentialList
        while (it.hasNext()) {
            int data = it.next();
            System.out.println(data);
        }
        System.out.println(c.contains(200));

        Collections.addAll(c, 400, 500);
        System.out.println(c);

        c.add(300);
        System.out.println(c);


    }
}
