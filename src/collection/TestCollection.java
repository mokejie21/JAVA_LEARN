package collection;

import java.sql.Connection;
import java.util.*;

public class TestCollection {
    public static void main(String[] args) {
        Collection<Integer> c = new ArrayList<>();
        c.add(100);
        c.add(200);
        c.add(300);
        System.out.println(c);

        System.out.println(c.contains(100));
        System.out.println(c.equals(200));
        System.out.println(c.hashCode());

        Object[] o = c.toArray();
        System.out.println(Arrays.toString(o));
        Object[] o2 = {34, null, "str"};
//        Collection c2 = new ArrayList();
//        c2.add(45);
//        c2.add("fan");
//        c2.add(null);
//        System.out.println(c2);
        Iterator<Integer> it = c.iterator();
        while (it.hasNext()) {
            Integer data = it.next();
            System.out.println(data);
        }

        for (Integer a : c) {
            System.out.println(a);
        }

        Collection<Integer> c3 = new ArrayList<>();
        c3.add(100);
        c3.add(345);
        c3.add(500);
        c3.add(200);

        System.out.println(c3.contains(c));

        c.retainAll(c3);
        System.out.println(c);


        c3.removeAll(c);
        System.out.println(c3);
        c.removeAll(c3);
        System.out.println(c);
        System.out.println(c.size());

    }
}
