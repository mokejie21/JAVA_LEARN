package collection;

import java.util.HashMap;

/**
 * @title: TestHashMap
 * @Date: 2022/2/14 10:01
 * @Version 1.0
 */
public class TestHashMap {

    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("01", 1);
        map.put("02", 1);
        map.put("03", 129);
        map.put(null, 1);
        map.put("05", 1);
        map.put("06", 1);
        map.put("07", 1);
        map.put("08", 1);
        map.put("09", 1);
        map.put("10", 1);
        map.put("11", 1);
        map.put("12", 1);
        map.put("13", 1);
        map.put("14", 1);
        map.put("15", 1);
        map.put("16", 1);
        System.out.println(map.toString());


    }

}
