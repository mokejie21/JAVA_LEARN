package collection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class TestList {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        list.add(100);
        list.add(200);
        list.add(300);
        list.add(200);
        list.add(null);
        list.add(2, 400);
        System.out.println(list.get(3));
        System.out.println(list);
        System.out.println(list.indexOf(200));
        System.out.println(list.lastIndexOf(200));
        System.out.println(list.remove(3));
        System.out.println(list);
        System.out.println(list.set(0, 30));
        System.out.println(list);
        System.out.println(list.remove(new Integer(200)));
        System.out.println(list);

        Iterator<Integer> it = list.iterator();
        while (it.hasNext()) {
            Integer data = it.next();
            System.out.println(data);
        }

        for (Integer x : list) {
            System.out.println(x);
        }

        for (int i = 0; i < list.size(); i++) {
            Integer data = list.get(i);
            System.out.println(data);
        }

        ListIterator<Integer> it2 = list.listIterator();
        while (it2.hasNext()) {
            Integer data = it2.next();
            System.out.println(data);
        }
        while (it2.hasPrevious()) {
            Integer data = it2.previous();
            System.out.println(data);
        }

    }
}
