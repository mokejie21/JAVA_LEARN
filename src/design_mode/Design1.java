package design_mode;

public class Design1 {
    public static void main(String[] args) {
        Person p = Person.show();
        p.method();
        Student s = Student.getStudent();
        s.name = "李明";
        System.out.println(s.name);
    }
}

class Person {//饿汉式

    private Person() {
    }

    private static Person p = new Person();

    static public Person show() {
        return p;
    }

    public void method() {
        System.out.println("hello");
    }
}

class Student {//懒汉式
    private static Student student;
    String name;

    private Student() {
    }

    synchronized public static Student getStudent() {

        if (student == null) {
            student = new Student();
        }
        return student;
    }
}
