package nettey;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * created by Fan
 * 2021/7/22
 */

public class Bio {
    public static void main(String[] args) throws IOException {

        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(2);
        ServerSocket serverSocket = new ServerSocket(2222);
        System.out.println("服务器已启动");
        while (true) {
            final Socket socket = serverSocket.accept();
            System.out.println("服务器已连接");
            newFixedThreadPool.execute(new Runnable() {
                @Override
                public void run() {
                    handler(socket);
                }
            });

        }

    }

    private static void handler(Socket socket) {
        try {
            byte[] bytes = new byte[1024];
            InputStream inputStream = socket.getInputStream();
            while (true) {
                int read = inputStream.read(bytes);
                if (read != -1) {
                    System.out.print(new String(bytes, 0, read));
                } else {
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("关闭连接");
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
