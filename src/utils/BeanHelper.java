package utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Ref;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @title: BeanHelper
 * @Date: 2022/3/6 17:10
 * @Version 1.0
 */
public class BeanHelper {

    private static final Logger logger = LoggerFactory.getLogger(BeanHelper.class);

    static class ReflectionInfo {
        Map<String, Method> readMap = new HashMap<>();
        Map<String, Method> writeMap = new HashMap<>();

        Method getReadMethod(String prop) {
            return prop == null ? null : readMap.get(prop.toLowerCase());
        }

        Method getWriteMethod(String prop) {
            return prop == null ? null : writeMap.get(prop.toLowerCase());
        }
    }

    protected static final Object[] NULL_ARGUMENTS = {};
    private static Map<String, ReflectionInfo> cache = new ConcurrentHashMap<>();

    private static BeanHelper beanHelper = new BeanHelper();

    public static BeanHelper getInstance() {
        return beanHelper;
    }

    public BeanHelper() {

    }

    public static List<String> getPropertys(Object bean) {
        return Arrays.asList(getInstance().getPropertiesAry(bean));
    }

    public String[] getPropertiesAry(Object bean) {
        ReflectionInfo reflectionInfo = cachedReflectionInfo(bean.getClass());
        Set<String> propertys = new HashSet<>();
        for (String key : reflectionInfo.writeMap.keySet()) {
            if (reflectionInfo.writeMap.get(key) != null) {
                propertys.add(key);
            }
        }
        return propertys.toArray(new String[0]);
    }

    public static Object getProperty(Object bean, String propertyName) {

        try {
            Method method = getInstance().getMethod(bean, propertyName, false);
            if (method == null) {
                return null;
            }
            return method.invoke(bean, NULL_ARGUMENTS);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            String errStr = "Failed to get property:" + propertyName;
            throw new RuntimeException(errStr, e);
        }
        //FIXME
        return null;
    }

    public static Object[] getPropertyValues(Object bean, String[] propertys) {
        Object[] result = new Object[propertys.length];
        try {
            Method[] methods = getInstance().getMethods(bean, propertys, false);
            for (int i = 0; i < propertys.length; i++) {
                if (propertys[i] == null || methods[i] == null) {
                    result[i] = null;
                } else {
                    result[i] = methods[i].invoke(bean, NULL_ARGUMENTS);
                }
            }
        } catch (Exception e) {
            String errStr = "Failes to get getPropertys from" + bean.getClass();
            throw new RuntimeException(errStr, e);
        }
        return result;

    }

    private Method[] getMethods(Object bean, String[] propertys) {
        return getInstance().getMethods(bean, propertys, true);
    }

    private Method[] getMethods(Object bean, String[] propertys, boolean isSetMethod) {
        Method[] methods = new Method[propertys.length];
        ReflectionInfo reflectionInfo = null;
        reflectionInfo = cachedReflectionInfo(bean.getClass());

        for (int i = 0; i < propertys.length; i++) {
            Method method = null;
            if (isSetMethod) {
                method = reflectionInfo.getWriteMethod(propertys[i]);

            } else {
                method = reflectionInfo.getReadMethod(propertys[i]);
            }
            methods[i] = method;
        }
        return methods;
    }


    private Method getMethod(Object bean, String propertyName, boolean isSetMethod) {
        Method method = null;
        ReflectionInfo reflectionInfo = cachedReflectionInfo(bean.getClass());

        if (isSetMethod) {
            reflectionInfo.getWriteMethod(propertyName);
        } else {
            method = reflectionInfo.getReadMethod(propertyName);
        }
        return method;

    }


    private ReflectionInfo cachedReflectionInfo(Class<?> beanCls) {
        return cachedReflectionInfo(beanCls, null);
    }

    private ReflectionInfo cachedReflectionInfo(Class<?> beanCls, List<PropDescriptor> pdescriptor) {
        String key = beanCls.getName();
        ReflectionInfo reflectionInfo = cache.get(key);
        if (reflectionInfo == null) {
            reflectionInfo = cache.get(key);
            if (reflectionInfo == null) {
                reflectionInfo = new ReflectionInfo();
                List<PropDescriptor> propDesc = new ArrayList<>();
                if (pdescriptor != null) {
                    propDesc.addAll(pdescriptor);
                } else {
                    propDesc = getPropertyDescriptors(beanCls);
                }
                for (PropDescriptor pd : propDesc) {
                    Method readMethod = pd.getReadMethod(beanCls);
                    Method writeMethod = pd.getWriteMethod(beanCls);
                    if (readMethod != null) {
                        reflectionInfo.readMap.put(pd.getName().toLowerCase(), readMethod);
                    }
                    if (writeMethod != null) {
                        reflectionInfo.readMap.put(pd.getName().toLowerCase(), writeMethod);
                    }
                }
                cache.put(key, reflectionInfo);
            }
        }
        return reflectionInfo;
    }

    public static void invokeMethod(Object bean, Method method, Object value) {
        try {
            if (method == null)
                return;
            Object[] arguments = {value};
            method.invoke(bean, arguments);
        } catch (Exception e) {
            String errStr = "Failed to set property:" + method.getName();
            throw new RuntimeException(errStr, e);
        }

    }

    public static void setProperty(Object bean, String propertyName, Object value) {
        try {
            Method method = getInstance().getMethod(bean, propertyName, true);
            if (method == null) return;
            Class<?>[] parameterTypes = method.getParameterTypes();
            if (parameterTypes[0].isAssignableFrom(boolean.class) || parameterTypes[0].isAssignableFrom(Boolean.class)) {
                if (value instanceof BigInteger) {
                    BigInteger intvalue = (BigInteger) value;
                    boolean booleanValue = intvalue.intValue() == 1;
                    method.invoke(bean, booleanValue);
                } else {
                    method.invoke(bean, value);
                }
            } else if (parameterTypes[0].isAssignableFrom(int.class) || parameterTypes[0].isAssignableFrom(Integer.class)) {
                int intValue = -1;
                if (value instanceof Boolean) {
                    if ((Boolean) value) {
                        intValue = 1;
                    } else {
                        intValue = 0;
                    }
                } else if (value instanceof Integer) {
                    intValue = (Integer) value;
                } else if (value instanceof Short) {
                    intValue = ((Short) value).intValue();
                } else if (value instanceof BigInteger) {
                    intValue = ((BigInteger) value).intValue();
                } else {
                    throw new RuntimeException("未知类型" + bean + "属性" + propertyName + "with value" + value);
                }
                method.invoke(bean, intValue);
            } else if (parameterTypes[0].isAssignableFrom(String.class)) {
                if (value instanceof BigDecimal) {
                    method.invoke(bean, value.toString());
                } else {
                    method.invoke(bean, value);
                }
            } else {
                method.invoke(bean, value);
            }
        } catch (IllegalArgumentException e) {
            String errStr = "Failed to set property: " + propertyName + " at bean: "
                    + bean.getClass().getName() + " with value:" + value + " type:"
                    + (value == null ? "null" : value.getClass().getName());
            // Logger.error(errStr, e);
            throw new IllegalArgumentException(errStr, e);
        } catch (Exception e) {
            String errStr = "Failed to set property: " + propertyName + " at bean: "
                    + bean.getClass().getName() + " with value:" + value;
            // Logger.error(errStr, e);
            throw new RuntimeException(errStr, e);
        }
    }

    public Method[] getAllGetMethod(Class<?> beanCls,String[] fieldNames){
        Method[] methods = null;
        ReflectionInfo reflectionInfo = null;
        ArrayList<Method> al = new ArrayList<>();
        reflectionInfo = cachedReflectionInfo(beanCls);
        for (String str : fieldNames) {
            al.add(reflectionInfo.getReadMethod(str));
        }
        return al.toArray(new Method[al.size()]);
    }

    private List<PropDescriptor> getPropertyDescriptors(Class<?> clazz) {
        List<PropDescriptor> descList = new ArrayList<>();
        List<PropDescriptor> superDescList = new ArrayList<>();
        List<String> propsList = new ArrayList<>();
        Class<?> propType = null;
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.getName().length() < 4) {
                continue;
            }
            if (method.getName().charAt(3) < 'A' || method.getName().charAt(3) > 'Z') {
                continue;
            }
            if (method.getName().startsWith("set")) {
                if (method.getParameterTypes().length != 1) {
                    continue;
                }
                if (method.getReturnType() != void.class) {
                    continue;
                }
                propType = method.getParameterTypes()[0];
            } else if (method.getName().startsWith("get")) {
                if (method.getParameterTypes().length != 0) {
                    continue;
                }
                propType = method.getReturnType();
            } else {
                continue;
            }

            String propname = method.getName().substring(3, 4).toLowerCase();

            if (method.getName().length() > 4) {
                propname = propname + method.getName().substring(4);
            }
            if (propname.equals("class")) {
                continue;
            }
            if (propsList.contains(propname)) {
                continue;
            } else {
                propsList.add(propname);
            }
            descList.add(new PropDescriptor(clazz, propType, propname));
        }

        Class<?> superClazz = clazz.getSuperclass();
        if (superClazz != null) {
            superDescList = getPropertyDescriptors(superClazz);
            descList.addAll(superDescList);
            if (!isBeanCached(superClazz)) {
                cachedReflectionInfo(superClazz, superDescList);
            }
        }
        return descList;

    }

    private boolean isBeanCached(Class<?> bean) {
        String key = bean.getName();
        ReflectionInfo cMethod = cache.get(key);
        if (cMethod == null) {
            cMethod = cache.get(key);
            return cMethod != null;
        }
        return true;
    }


}
