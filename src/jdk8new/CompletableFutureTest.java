package jdk8new;

import java.util.concurrent.CompletableFuture;

/**
 * @title: CompletableFutureTest
 * @Date: 2022/3/1 21:56
 * @Version 1.0
 */

public class CompletableFutureTest {

    public static void main(String[] args) {
        CompletableFuture<Void> test02 = CompletableFuture.runAsync(() -> {
            System.out.println("test02");
        });

        CompletableFuture<Boolean> supplyAsync = CompletableFuture.supplyAsync(() -> {
            System.out.println("test01");
            return true;
        });


        try {
            test02.get();
            supplyAsync.get();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
