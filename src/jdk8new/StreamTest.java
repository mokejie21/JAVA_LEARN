package jdk8new;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * created by Fan
 * 2021/8/28
 *
 * @author user
 */
public class StreamTest {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("red", "black", "white", "", "red");
//        List<String> collect = list.stream().distinct().collect(Collectors.toList());
//        List<String> collect1 = list.stream().map(str -> str.concat("java")).collect(Collectors.toList());
//
//        List<String> collect2 = list.stream().sorted().distinct().collect(Collectors.toList());
//        Integer[] a = new Integer[10];
//        for (int i = 0; i < a.length; i++) {
//            a[i] = new Random().nextInt(100);
//        }
//        List<Integer> stream = Arrays.stream(a).collect(Collectors.toList());
//        System.out.println(stream);
//        Arrays.stream(a).forEach(ao ->{
//            System.out.println(a);
//        });

        list.stream().filter(x -> !"".equals(x)).distinct().collect(Collectors.toList()).forEach(System.out::println);


    }
}
