package list;


/**
 * created by Fan
 * 2021/8/28
 */
public class List<T> {
    private Node first;
    private int N;

    private class Node {
        T item;
        Node next;
    }

    public int size() {
        return N;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public void push(T item) {
        Node oldFirst = first;
        first = new Node();
        first.item = item;
        first.next = oldFirst;
        N++;
    }

    public T pop() {
        T item = first.item;
        first = first.next;
        N--;
        return item;
    }

    @Override
    public String toString() {
        return "List{" +
                "item=" + first.item +
                '}';
    }
}
