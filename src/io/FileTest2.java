package io;

import java.io.File;
import java.util.Scanner;

public class FileTest2 {
    public static void main(String[] args) {
        System.out.println("请输入文件夹的路径:");
        String path = new Scanner(System.in).nextLine();
        File dir = new File(path);
        long sum = sizeOf(dir);
        System.out.println("文件夹资源大小为:" + sum);
    }

    private static long sizeOf(File dir) {
        File[] a = dir.listFiles();
        long sum = 0;
        for (File f : a) {
            if (f.isFile()) {
                sum += f.length();
            } else if (f.isDirectory()) {
                sum += sizeOf(f);
            }
        }
        return sum;
    }
}
