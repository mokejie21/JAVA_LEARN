package io;

import java.io.File;
import java.util.Scanner;

/**
 * created by Fan
 * 2021/8/7
 */
public class FileDelete {
    public static void main(String[] args) {
        String targetDir = "C:/Users/13521/Desktop/java";


        File file = new File(targetDir);
        fileDelete(file);
        System.out.println(targetDir + " is deleted ok!");

    }

    private static void fileDelete(File file) {

        File[] files = file.listFiles();
        for (File value : files) {
            if (value.isFile()) {
                value.delete();
            } else if (value.isDirectory()) {
                fileDelete(value);
                value.delete();
            }
        }


    }
}
