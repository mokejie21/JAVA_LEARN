package io;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class FileCopy {
    public static void main(String[] args) throws Exception {
        String fromPath = "C:/Users/13521/Desktop/";
        String toPath = "D:/Java/Data/";

        String toDirName = getDirName(toPath);//创建文件夹
        System.out.println("输入文件名:");
        String fileName = new Scanner(System.in).nextLine();
        String fileRealName = fromPath + fileName;
        toDirName = toDirName + "/" + fileName;
        copyOf(fileRealName, toDirName);
        System.out.println("复制完成!");

    }

    private static void copyOf(String from, String to) throws Exception {
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(from));
        BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(to));
        int b = 0;
        byte[] buf = new byte[8192];
        while ((b = in.read(buf)) != -1) {
            out.write(buf);
        }
        in.close();
        out.close();
    }

    public static String getDirName(String localPath) {
        String virdir = new SimpleDateFormat("/yyyy/MM/dd").format(new Date());
        String dirName = localPath + virdir;
        File file = new File(dirName);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath();
    }
}
