package io;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class FileTest {
    public static void main(String[] args) throws IOException {
        String url = "D:\\iotest\\1.txt";
        String url2 = "D:\\iotest\\2.txt";
        String url3 = "D:\\iotest\\a";
        String url4 = "D:\\iotest\\a\\b\\c";
        String url5 = "D:\\iotest\\3.txt";
        String url6 = "D:\\iotest\\4.txt";

        File file = new File(url);
        System.out.println(file.length());
        System.out.println(file.exists());
        System.out.println(file.isFile());
        System.out.println(file.getName());
        System.out.println(file.getParent());
        System.out.println(file.getAbsolutePath());
        file = new File(url2);
        System.out.println(file.createNewFile());
        file = new File(url3);
        System.out.println(file.mkdir());
        file = new File(url4);
        System.out.println(file.mkdirs());
        System.out.println(file.delete());

        File[] files = file.listFiles();
        System.out.println(Arrays.toString(files));

        File[] b = new File[3];
        b[0] = new File(url2);
        b[1] = new File(url4);
        b[2] = new File(url5);
        for (File value : b) {
            System.out.println(value);
            System.out.println(value.getName());
        }
        System.out.println("-----------------");
        File dir = new File("D:\\iotest");
        System.out.println(dir);
        File[] a = dir.listFiles();
        long sum = 0;
        for (File f : a) {
            if (f.isDirectory()) {
                System.out.println(f.getName());
            }
            if (f.isFile()) {
                sum += f.length();
            }
        }
        System.out.println("文件夹是" + dir.getAbsolutePath() + "文件大小:" + sum);


    }
}
