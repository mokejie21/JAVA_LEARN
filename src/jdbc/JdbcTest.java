package jdbc;

import java.sql.*;

/**
 * created by Fan
 * 2021/7/26
 */
public class JdbcTest {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/jdbctest?characterEncoding=utf8&serverTimezone=Asia/Shanghai";
            Connection connection = DriverManager.getConnection(url, "root", "root");
            String sql = "select * from user where name =?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "amy");
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                for (int i = 1; i < 4; i++) {
                    System.out.print(resultSet.getString(i));
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
