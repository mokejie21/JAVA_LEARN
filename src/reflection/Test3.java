package reflection;

import reflection.mode.Human1;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Test3 {
    public static void main(String[] args) throws Exception {
//                method();//所有方法
        method2();//所有属性
    }

    private static void method2() throws Exception {
        Class c = Human1.class;
        Field[] f1 = c.getDeclaredFields();
        for (Field f : f1) {
            Object obj = c.newInstance();
            if (f.getName().equals("name")) {
                f.set(obj, "张三");
                System.out.println(f.get(obj));
            }
            if (f.getName().equals("age")) {
                f.setAccessible(true);
                f.set(obj, 664);
                System.out.println(f.get(obj));
            }
        }

    }

    private static void method() throws Exception {
        Class c = Human1.class;
//                Method[] m1 = c.getMethods();
        Method[] m2 = c.getDeclaredMethods();

//                System.out.println(Arrays.toString(m1));
        for (Method m : m2) {
            Object obj = c.newInstance();
            if (m.getName().equals("test2")) {
                m.setAccessible(true);
                m.invoke(obj, "hello");
            }
        }
    }

}
