package reflection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

public class Test2 {
    public static void main(String[] args) throws Exception {
        Class c = Demo.class;
        Method[] a = c.getMethods();
        for (Method m : a) {
            Controller co = m.getAnnotation(Controller.class);
            if (co != null) {
                Object obj = c.newInstance();
                m.invoke(obj);
            }

        }
    }
}

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface Controller {

}

class Demo {
    String name = "jack";

    @Controller
    public void method() {
        System.out.println(123);
    }

    @Controller
    public void method1() {
        System.out.println(3);
    }
}
