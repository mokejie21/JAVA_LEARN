package reflection;


import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import reflection.mode.Human;

/**
 * 测试反射
 */
public class Test1 {
    public static void main(String[] args) throws Exception {
//                method();//操作成员变量
//                method2();//操作成员方法
        method3();//操作构造方法
    }

    private static void method3() throws Exception {
        Class c = Human.class;
        Constructor[] a = c.getConstructors();
        for (Constructor co : a) {
            Class[] cc = co.getParameterTypes();
            if (cc.length == 0) {
                Object o = co.newInstance();
            }
            if (cc.length != 0) {
                Object o = co.newInstance("tony", 20, 10000);
                System.out.println(o);
            }
        }
    }

    private static void method2() throws Exception {
        Class c = Class.forName("reflection.mode.Human");
        Method[] m = c.getDeclaredMethods();
        for (Method m1 : m) {
            System.out.println(m1.getName());
            if (m1.getName().equals("test1")) {
                Object obj = c.newInstance();
                m1.invoke(obj);
            }
        }
    }

    private static void method() throws Exception {
        Class c = Human.class;//获取Class对象
        Field[] a = c.getFields();
        for (Field f : a) {
            System.out.println(f.getName());
            if (f.getName().equals("name")) {
                Object obj = c.newInstance();
                f.set(obj, "jack");
                System.out.println(f.get(obj));
            }
        }
    }
}
