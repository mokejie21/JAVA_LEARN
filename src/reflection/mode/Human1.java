package reflection.mode;

/**
 * 测试反射用例
 */
public class Human1 {
    public String name;
    private int age;
    double salary;

    public Human1(String name, int age, double salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    public Human1() {
    }

    public void test1() {
        System.out.println(1);
    }

    private void test2(String s) {
        System.out.println(s);
    }

    void test3(int a) {
        System.out.println(a);
    }


}
