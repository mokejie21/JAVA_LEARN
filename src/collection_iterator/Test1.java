package collection_iterator;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Test1 {
    public static void main(String[] args) {
        List<Integer> list = new Mode().getList();
        Iterator<Integer> it = list.iterator();
        while (it.hasNext()) {
            Integer data = it.next();
            System.out.print(data + " ");
        }
        System.out.println();

        for (int i = 0; i < list.size(); i++) {
            Integer data = list.get(i);
            System.out.print(data + " ");
        }
        System.out.println();

        for (Integer data : list) {
            System.out.print(data + " ");
        }
        System.out.println();

        ListIterator<Integer> it2 = list.listIterator();
        while (it2.hasNext()) {
            Integer data = it2.next();
            System.out.print(data + " ");
        }
        System.out.println();
        while (it2.hasPrevious()) {
            Integer data = it2.previous();
            System.out.print(data + " ");
        }


    }

}
