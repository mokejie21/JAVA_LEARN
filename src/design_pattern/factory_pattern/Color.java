package design_pattern.factory_pattern;

/**
 * created by Fan
 */
public interface Color {
    void buy();
}
