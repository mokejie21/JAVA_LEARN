package design_pattern.factory_pattern;

/**
 * created by Fan
 */
public class FactoryParrternDemo {
    public static void main(String[] args) {
        ColorFactory factory = new ColorFactory();
        Color colorB = factory.colorFactory("blue");
        colorB.buy();
        Color colorR = factory.colorFactory("red");
        colorR.buy();
    }
}
