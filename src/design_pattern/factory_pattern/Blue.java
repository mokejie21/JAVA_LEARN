package design_pattern.factory_pattern;

/**
 * created by Fan
 */
public class Blue implements Color {
    @Override
    public void buy() {
        System.out.println("我买了蓝色的帽子");
    }
}
