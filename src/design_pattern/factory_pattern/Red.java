package design_pattern.factory_pattern;

/**
 * created by Fan
 */
public class Red implements Color {
    @Override
    public void buy() {
        System.out.println("我买了红色的帽子");
    }
}
