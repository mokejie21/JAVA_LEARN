package design_pattern.factory_pattern;

/**
 * created by Fan
 */
public class ColorFactory {
    public Color colorFactory(String color) {
        if (color == null) {
            return null;
        }
        if (color.equalsIgnoreCase("BLUE")) {
            return new Blue();
        } else if (color.equalsIgnoreCase("RED")) {
            return new Red();
        }
        return null;
    }
}
