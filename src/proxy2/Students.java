package proxy2;

public class Students implements Person {
    private String name;

    public Students(String name) {
        this.name = name;
    }

    @Override
    public void giveTask() {
        System.out.println(name + "交语文作业");
    }

}
