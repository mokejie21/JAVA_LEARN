package proxy2;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class ProxyTest {
    public static void main(String[] args) {
        Person lin = new Students("王小明");
        InvocationHandler stuHandler = new StuInvocationHandler<Person>(lin);

        Person stuProxy =
                (Person) Proxy.newProxyInstance(Person.class.getClassLoader(), new Class<?>[]{Person.class}, stuHandler);
        stuProxy.giveTask();
    }
}
