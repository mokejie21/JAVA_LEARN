package ioc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * created by Fan
 * 2021/7/26
 */
public class SpringContext {
    private List<Bean> beanFactory = new ArrayList<>();

    public SpringContext() throws Exception {
        Bean bean = new Bean();
        bean.setName("hello");
        bean.setPath("ioc.cn.pojo.Hello");
        beanFactory.add(bean);
        init();
    }

    private final Map<String, Object> factoryBeanObject = new ConcurrentHashMap<>();

    public void init() throws Exception {
        for (Bean bean : beanFactory) {
            String key = bean.getName();
            String path = bean.getPath();
            Object value = Class.forName(path).newInstance();
            factoryBeanObject.put(key, value);
        }
    }

    public Object getBean(String name) {
        return factoryBeanObject.get(name);
    }
}
