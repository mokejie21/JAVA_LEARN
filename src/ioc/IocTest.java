package ioc;

import ioc.cn.pojo.Hello;

import java.beans.beancontext.BeanContext;

/**
 * created by Fan
 * 2021/7/26
 */
public class IocTest {
    public static void main(String[] args) throws Exception {
        SpringContext context = new SpringContext();
        Hello hello = (Hello) context.getBean("hello");
        hello.hi();
    }
}
