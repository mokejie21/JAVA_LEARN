package proxy;

/**
 * 静态代理
 */

public class StudentProxy implements Person {
    Students stu;

    public StudentProxy(Person stu) {
        //
        if (stu.getClass() == Students.class) {
            this.stu = (Students) stu;
        }
    }

    @Override
    public void giveTask() {
        stu.giveTask();

    }
}
