package proxy;

/**
 * 静态代理测试类
 */

public class StaticStudentProxy {
    public static void main(String[] args) {
        Person lin = new Students("李晓明");
        Person moniter = new StudentProxy(lin);
        moniter.giveTask();
        lin.giveTask();
    }
}
