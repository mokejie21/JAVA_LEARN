package proxy;

/**
 * @author user
 */

public interface Person {
    /**
     * @giveTask:被代理实现的方法
     */
    void giveTask();
}
