public class TestExtends {
    public static void main(String[] args) {
        Fa f = new Son();
        Son.name = "王小明";
        System.out.println(Son.name);
        Son.method();
    }
}

class Fa {
    static String name;

    public static void method() {//可以被继承,不能被重写
        System.out.println("静态资源继承的测试");
    }
}

class Son extends Fa {
    public static void method() {
        System.out.println("静态资源能否被重写");
    }

}
