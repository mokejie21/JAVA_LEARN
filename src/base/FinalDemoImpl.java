package base;

/**
 * @title: FinalDemoImpl
 * @Date: 2022/2/16 14:49
 * @Version 1.0
 */
public class FinalDemoImpl implements FinalDemo, FinalDemo.Test {
    @Override
    public void eat() {

    }

    @Override
    public void play() {
        FinalDemo.sleep();
        FinalDemoImpl finalDemo = new FinalDemoImpl();
        finalDemo.paid();

    }

}
