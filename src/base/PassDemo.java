package base;

import java.util.ArrayList;

/**
 * @title: PassDemo
 * @Date: 2022/2/9 11:05
 * @Version 1.0
 */
public class PassDemo {
    //值传递，引用传递demo
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(3);
        list.add(4);
        test01(list);
        //打印显示[]为引用传递,显示为[2,3,4]为值传递
        System.out.println(list.toString());

        String name = "aaddd";
        test02(name);
        //打印结果显示为"zxc"则为引用传递,为"aaddd"为值传递
        System.out.println(name);
    }

    private static void test02(String name) {
        name = "zxc";
    }

    private static void test01(ArrayList<Integer> list) {
        list = new ArrayList<>();
    }

    //总结:值传递传递的是参数的副本,修改副本对原参数的值无影响;引用传递的"引用"与引用类型的引用不一样,这里指的是地址和指针

}
