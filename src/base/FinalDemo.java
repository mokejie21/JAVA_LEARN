package base;

/**
 * created by Fan
 * 2022/2/16
 */
public interface FinalDemo {
    //默认为常量
    String name = "";


    //    private interface test{
//
//    }
//      protected interface test{
//
//    }
//    final interface test{
//
//    }
    //接口不可以使用private和protected修饰,不能使用final修饰,不能使用static修饰；内部接口可以使用static修饰（默认不加）

    static interface Test {
        void play();
    }

    void eat();

    //final void sleep();
    //抽象方法不可以使用final修饰
    //接口中可以有静态方法
    static void sleep() {

    }

    //接口中可以有default修饰的普通方法
    default void paid() {

    }


    /**
     * 总结：
     * 接口中的类变量均为常量；
     * 接口中的方法可以为抽象方法，被static,default修饰的方法；
     * 接口可以被public，default修饰范围，内部接口可以使用static修饰（默认不加）；
     * 接口不能被private，protected修饰，不能被final修饰
     */
}
