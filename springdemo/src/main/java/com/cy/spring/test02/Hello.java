package com.cy.spring.test02;

/**
 * created by Fan
 */
public class Hello {
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Hello{" +
                "name='" + name + '\'' +
                '}';
    }
}
