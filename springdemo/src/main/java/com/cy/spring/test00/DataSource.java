package com.cy.spring.test00;

import com.cy.spring.test01.Data;

/**
 * created by Fan
 */
public class DataSource {
    private Data source;

    public DataSource(Data source) {
        this.source = source;
    }


    public void test() {
        this.source.dataFrom();
    }
}
