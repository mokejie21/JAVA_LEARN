package com.cy.spring;

import com.cy.spring.test00.DataSource;
import com.cy.spring.test01.Oraca;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * created by Fan
 */
@SpringBootTest
public class TestSpring {
    @Test
    public void testSpring() {
        new DataSource(new Oraca()).test();
    }
}
