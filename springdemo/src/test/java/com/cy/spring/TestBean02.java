package com.cy.spring;

import com.cy.spring.test02.Hello;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * created by Fan
 */
public class TestBean02 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        Hello hello = context.getBean("hello", Hello.class);
        System.out.println(hello);
    }
}
