package com.cy.spring;

import com.cy.spring.test01.Data;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * created by Fan
 */
public class TestBean {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        Data mySpring = context.getBean("mySpring", Data.class);
        mySpring.dataFrom();
    }

}
