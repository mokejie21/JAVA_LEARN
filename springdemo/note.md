```
bean xml配置文件
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        https://www.springframework.org/schema/beans/spring-beans.xsd">
    <bean id="oraca" class="com.cy.spring.test01.Oraca"/>
    <bean id="mySpring" class="com.cy.spring.test01.MySpring"/>
    <bean id="hello" class="com.cy.spring.test02.Hello">
        <property name="name" value="spring"/>
    </bean>
</beans>
```
说明:

