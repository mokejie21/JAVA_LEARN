package com.cy.redis.pojo;

/**
 * created by Fan
 *
 * @author user
 */
public class Blog {
    private String id;
    private String title;

    public Blog(String id, String title) {
        this.id = id;
        this.title = title;
        System.out.println("Blog(String,String)");
    }

    public Blog() {
        System.out.println("Blog()");
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
