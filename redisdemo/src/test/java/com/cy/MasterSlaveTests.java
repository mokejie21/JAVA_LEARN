package com.cy;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

/**
 * created by Fan
 */
@SpringBootTest
public class MasterSlaveTests {
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void master() {
        ValueOperations ops = redisTemplate.opsForValue();
        ops.set("role", "master6379");
        Object role = ops.get("role");
        System.out.println(role);
    }

    @Test
    void slave() {
        ValueOperations ops = redisTemplate.opsForValue();
        Object role = ops.get("role");
        System.out.println(role);
    }
}
