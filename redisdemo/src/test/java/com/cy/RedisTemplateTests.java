package com.cy;

import com.cy.redis.pojo.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * created by Fan
 */
@SpringBootTest
public class RedisTemplateTests {
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void testConnetion() {
        //RedisTemplate存储数据时会默认基于JDK的序列化机制,将数据序列化以后再存储
        String con = redisTemplate.getConnectionFactory().getConnection().ping();
        //读取时,进行反序列化
        System.out.println(con);
    }


    @Test
    void testFlushed() {
        redisTemplate.execute(new RedisCallback() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                connection.flushAll();
                return "flush ok";
            }
        });
    }


    @Test
    void testHashOper() {
        HashOperations hashOperations = redisTemplate.opsForHash();
        Map<String, String> blog = new HashMap<>();
        blog.put("id", "1");
        blog.put("title", "hello");
        hashOperations.putAll("blog", blog);
        hashOperations.put("blog", "content", "world");
        Object content = hashOperations.get("blog", "content");
        System.out.println(content);
        blog = hashOperations.entries("blog");
        System.out.println(blog);
    }

    @Test
    void testStringOper02() {
        ValueOperations valueOperations = redisTemplate.opsForValue();
        Blog blog = new Blog("1", "con");

        valueOperations.set("blog", blog);
        Blog blog1 = (Blog) valueOperations.get("blog");
        System.out.println(blog1);
    }

    @Test
    void testStringOper() {
        ValueOperations valueOperations = redisTemplate.opsForValue();
        valueOperations.set("animal", "tiger");
        System.out.println(valueOperations.get("animal"));
    }

    @Test
    public void testSetData() {
        SetOperations setOperations = redisTemplate.opsForSet();
        setOperations.add("item", "A", "B", "A");
        Set item = setOperations.members("item");
        System.out.println("setKeys=" + item);
    }

    @Test
    public void testListData() {

        ListOperations listOperations = redisTemplate.opsForList();
        listOperations.leftPush("item", "java");
        listOperations.leftPush("item", "python");
        listOperations.leftPush("item", "c语言");
        listOperations.set("item", -1, "c++");
        List item = listOperations.range("item", 0, -1);
        System.out.println(item);

    }

    @Test
    public void testHashData() {
        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.put("phone", "color", "blue");
        hashOperations.put("phone", "name", "mi");
        Object o = hashOperations.get("phone", "name");
        Boolean aBoolean = hashOperations.hasKey("phone", "name");

        Map phone = hashOperations.entries("phone");
        System.out.println(o);
        System.out.println(aBoolean);
        System.out.println(phone);

    }


}
