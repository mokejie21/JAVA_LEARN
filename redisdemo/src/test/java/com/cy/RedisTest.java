package com.cy;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.*;

import java.util.*;

/**
 * created by Fan
 */
@SpringBootTest
public class RedisTest {

    @Test
    public void TestJedis() {

        Jedis jedis = new Jedis("192.168.126.128", 6379);
        System.out.println(jedis.ping());
        jedis.set("name", "jack");
        jedis.set("year", "10");
        System.out.println("set ok");
        String name = jedis.get("name");
        String year = jedis.get("year");
        System.out.println(String.format("name=%s;year=%s", name, year));
        jedis.incr("year");
        jedis.incrBy("year", 2);
        System.out.println(jedis.strlen("name"));
    }

    @Test
    public void TestJedisPool() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(200);
        JedisPool jedisPool = new JedisPool(jedisPoolConfig, "192.168.126.128", 6379);
        Jedis jedis = jedisPool.getResource();
        System.out.println(jedis.get("name"));
        jedisPool.close();

    }

    @Test
    public void testRedisHash() {
        Jedis jedis = new Jedis("192.168.126.128", 6379);
        Map<String, String> blogs = new HashMap<>();
        blogs.put("title", "java 是世界上最好的编程语言");
        blogs.put("createTime", "2021-07-09");
        jedis.hset("blog", blogs);
        List text = jedis.hmget("blog", "title", "createTime");
        System.out.println(text);
    }

    @Test
    public void testList01() {
        Jedis jedis = new Jedis("192.168.126.128", 6379);
        jedis.flushAll();
        jedis.rpush("item", "java", "c", "python");

        System.out.println(jedis.llen("item"));
        Long n = jedis.llen("item");
        List<String> item = jedis.lpop("item", n.intValue());
        System.out.println(item);
        jedis.close();
    }

    @Test
    public void testList02() {
        Jedis jedis = new Jedis("192.168.126.128", 6379);
        jedis.flushAll();
        jedis.lpush("item", "java", "c", "python");

        List<String> list = jedis.brpop(30, "item", "3");
        System.out.println(list);
        jedis.close();
    }

    //测试redis集群部署
    @Test
    void testJedisCluster() {
        Set<HostAndPort> nodes = new HashSet<>();
        nodes.add(new HostAndPort("192.168.126.128", 8010));
        nodes.add(new HostAndPort("192.168.126.128", 8011));
        nodes.add(new HostAndPort("192.168.126.128", 8012));
        nodes.add(new HostAndPort("192.168.126.128", 8013));
        nodes.add(new HostAndPort("192.168.126.128", 8014));
        nodes.add(new HostAndPort("192.168.126.128", 8015));
        JedisCluster jedisCluster = new JedisCluster(nodes);
        //使用jedisCluster操作redis
        for (int i = 0; i < 100; i++) {
            jedisCluster.set("test-" + i, String.valueOf(i));
        }


        jedisCluster.close();
    }


}
