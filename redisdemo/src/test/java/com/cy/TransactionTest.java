package com.cy;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

/**
 * created by Fan
 */
@SpringBootTest
public class TransactionTest {
    @Test
    void testTx() {
        Jedis jedis = new Jedis("192.168.126.128", 6379);
        Transaction multi = jedis.multi();
        try {
            multi.set("x", "34");
            multi.set("y", "45");
            multi.exec();
        } catch (Exception e) {
            e.printStackTrace();
            multi.discard();
        } finally {
            multi.close();
        }
    }

}
