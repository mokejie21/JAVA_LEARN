package com.cy;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.concurrent.TimeUnit;

//import java.util.concurrent.TimeUnit;

/**
 * created by Fan
 */
@SpringBootTest
public class StringRedisTemplateTests {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    public void testRedisStringOper() throws Exception {

        //获取用于操作字符串的值对象
        ValueOperations<String, String> valueOperations
                = stringRedisTemplate.opsForValue();
        //向redis中存储数据
        valueOperations.set("ip", "192.168.126.128");
        valueOperations.set("state", "1", 1, TimeUnit.SECONDS);
        valueOperations.decrement("state");
        // Thread.sleep(2000);
        //从redis中取数据
        String ip = valueOperations.get("ip");
        System.out.println("ip=" + ip);
        String state = valueOperations.get("state");
        System.out.println("state=" + state);
    }
}
