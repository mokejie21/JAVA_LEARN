package com.cy.demo01;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * created by Fan
 */
@Configuration
@ComponentScan("com.cy.demo01")
public class BeanConfig {
    @Bean
    public User getUser() {
        return new User();
    }
}
