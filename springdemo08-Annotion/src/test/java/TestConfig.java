import com.cy.demo01.BeanConfig;
import com.cy.demo01.User;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * created by Fan
 */
public class TestConfig {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BeanConfig.class);
        User user = context.getBean("getUser", User.class);
        System.out.println(user);
    }
}
