package com.cy.mapper;


import com.cy.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {
    List<User> getAll();

    User getOne();

    List<User> getUserByTel(@Param("tel") String tel);

    List<User> getUser(String name);
}
