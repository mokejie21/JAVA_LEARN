package com.cy.demo;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.List;


import com.cy.mapper.UserMapper;

import com.cy.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class MybatisDemo {
    public static void main(String[] args) throws IOException {
        InputStream in = Resources.getResourceAsStream("mybatis-config.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(in);
        SqlSession session = sqlSessionFactory.openSession();
        UserMapper userMapper = session.getMapper(UserMapper.class);
        List<User> userList = userMapper.getAll();
        for (User user : userList) {
            System.out.println(user);
        }
//        List<User> userList = session.selectList("userMapper.getAll");
//        System.out.println(userList);
//        System.out.println(userMapper.getOne());
//        System.out.println(userMapper.getUserByAddr("上海"));
        System.out.println(userMapper.getUser(null));


    }


    void jdbcTest(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
             connection = DriverManager.getConnection("");
             statement = connection.createStatement();
             resultSet = statement.executeQuery("");
             while (resultSet.next()){

             }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }



    }


}
