package com.cy.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 抖音用户表实体类
 *
 * @author tianlihu
 */
@ContentRowHeight(20)
@HeadRowHeight(20)
@ColumnWidth(20)
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("users")
@ApiModel(value = "Users对象", description = "抖音用户表")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @ExcelProperty("")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("用户唯一ID")
    @ExcelProperty("用户唯一ID")
    @TableField("external_id")
    private String externalId;

    @ApiModelProperty("昵称")
    @ExcelProperty("昵称")
    @TableField("nickname")
    private String nickname;

    @ApiModelProperty("头像")
    @ExcelProperty("头像")
    @TableField("avatar")
    private String avatar;

    @ApiModelProperty("手机号")
    @ExcelProperty("手机号")
    @TableField("telephone")
    private String telephone;

    @ApiModelProperty("token")
    @ExcelProperty("token")
    @TableField("token")
    private String token;

    @ApiModelProperty("抖音session_key")
    @ExcelProperty("抖音session_key")
    @TableField("dy_session_key")
    private String dySessionKey;

    @ApiModelProperty("字节openid")
    @ExcelProperty("字节openid")
    @TableField("dy_openid")
    private String dyOpenid;

    @ApiModelProperty("字节unionid")
    @ExcelProperty("字节unionid")
    @TableField("dy_unionid")
    private String dyUnionid;

    @ApiModelProperty("微信openid")
    @ExcelProperty("微信openid")
    @TableField("wx_openid")
    private String wxOpenid;

    @ApiModelProperty("微信unionid")
    @ExcelProperty("微信unionid")
    @TableField("wx_unionid")
    private String wxUnionid;

    @ApiModelProperty("支付宝token")
    @ExcelProperty("支付宝token")
    @TableField("ali_refresh_token")
    private String aliRefreshToken;

    @ApiModelProperty("支付宝authCode")
    @ExcelProperty("支付宝authCode")
    @TableField("ali_code")
    private String aliCode;

    @ApiModelProperty("支付宝用户ID")
    @ExcelProperty("支付宝用户ID")
    @TableField("ali_user_id")
    private String aliUserId;

    @ExcelProperty("")
    @TableField("created_at")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createdAt;

    @ExcelProperty("")
    @TableField("updated_at")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updatedAt;

    @ApiModelProperty("城市id")
    @ExcelProperty("城市id")
    @TableField("city_id")
    private Integer cityId;
    /**
     * 纬度
     */
    @TableField(exist = false)
    private BigDecimal longitude;
    /**
     * 纬度
     */
    @TableField(exist = false)
    private BigDecimal latitude;
    /**
     * 微信appid
     */
    @JsonIgnore
    private String wxAppid;
    /**
     * 是否团长
     */
    @TableField(exist = false)
    @JsonIgnore
    private Boolean head = Boolean.FALSE;

    /**
     * 用户性别，0: 未知；1:男性；2:女性(douyin)
     **/
    private Integer gender;

    @TableField(exist = false)
    private String cityName;
}
