package com.cy;


import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * created by Fan
 */
public class User {
    private String name;
    private Address address;
    private String[] books;
    private List<String> hobby;
    private Map<String, String> card;
    private String wife;
    private Properties info;

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setBooks(String[] books) {
        this.books = books;
    }

    public void setHobby(List<String> hobby) {
        this.hobby = hobby;
    }

    public void setCard(Map<String, String> card) {
        this.card = card;
    }

    public void setWife(String wife) {
        this.wife = wife;
    }

    public void setInfo(Properties info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", address=" + address +
                ", books=" + Arrays.toString(books) +
                ", hobby=" + hobby +
                ", card=" + card +
                ", wife='" + wife + '\'' +
                ", info=" + info +
                '}';
    }
}
