package com.cy.test;

import com.cy.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * created by Fan
 */
public class TestSpring {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        UserService userServiceImpl = context.getBean("userServiceImpl", UserService.class);
        userServiceImpl.getUser();
    }
}
