package com.cy.service;

import com.cy.dao.UserDao;

/**
 * created by Fan
 */
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    public void setUser(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void getUser() {
        userDao.dataFrom();
    }


}
