package com.cy.demo01;

/**
 * created by Fan
 */

public class Proxy implements Rent {
    private Host host;

    public Proxy(Host host) {
        this.host = host;
    }

    public Proxy() {
    }

    @Override
    public void rent() {
        host.rent();
        fee();
    }

    private void fee() {
        System.out.println("收费");
    }
}
