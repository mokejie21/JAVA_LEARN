package com.cy.demo02;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * created by Fan
 * 2021/7/19
 */
public class ProxyHandler implements InvocationHandler {
    private Rent rent;

    public void setRent(Rent rent) {
        this.rent = rent;
    }

    public Object getproxy() {
        Object proxy = Proxy.newProxyInstance(this.getClass().getClassLoader(),
                rent.getClass().getInterfaces(), this);
        return proxy;
    }

    @Override
    public Object invoke(Object proxy,
                         Method method, Object[] args) throws Throwable {
        fee();
        Object result = method.invoke(rent, args);
        return result;
    }

    public void fee() {
        System.out.println("费用很贵");
    }
}
