package com.cy.demo02;

/**
 * created by Fan
 * 2021/7/19
 */
public class Client {
    public static void main(String[] args) {
        Rent rent = new Host();
        ProxyHandler proxyHandler = new ProxyHandler();
        proxyHandler.setRent(rent);
        Rent getproxy = (Rent) proxyHandler.getproxy();
        getproxy.rent();
    }
}
