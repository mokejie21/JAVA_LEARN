package com.cy;

import com.cy.demo01.Host;
import com.cy.demo01.Proxy;

/**
 * created by Fan
 */
public class TestProxy {
    public static void main(String[] args) {
        Proxy proxy = new Proxy(new Host());
        proxy.rent();
    }
}
