package com.cy.test;

import com.cy.aop.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * created by Fan
 * 2021/7/20
 */
public class TestAop {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
        UserService proxy = (UserService) context.getBean("userService");
        proxy.findUser();
    }
}
