package com.cy.aop.asp;

/**
 * created by Fan
 * 2021/7/20
 * 方式二：利用切面
 */
public class Accept {
    public void before() {
        System.out.println("------方法执行前-------");
    }

    public void after() {
        System.out.println("------方法执行后-------");
    }
}
