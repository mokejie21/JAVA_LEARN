package com.cy.aop.log;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * created by Fan
 * 2021/7/20
 */
public class Log implements MethodBeforeAdvice {
    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("执行" + method.getDeclaringClass().getName() + "的" + method.getName());
    }
}
