package com.cy.aop.annotation;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;

/**
 * created by Fan
 * 2021/7/20
 * 方式三：使用注解
 */
@Aspect
public class Annotation {

    @Before("execution(* com.cy.aop.service.implement.UserServiceImpl.findUser())")
    public void before() {
        System.out.println("--方法执行前---");
    }

    @After("execution(* com.cy.aop.service.implement.UserServiceImpl.*(..))")
    public void after() {
        System.out.println("--方法执行后---");
    }

    @Around("execution(* com.cy.aop.service.implement.UserServiceImpl.*(..))")
    public void around(ProceedingJoinPoint joinPoint) throws Throwable {
        Signature signature = joinPoint.getSignature();
        System.out.println(signature);
        System.out.println("环绕前");
        Object proceed = joinPoint.proceed();
        System.out.println("环绕后");

    }

}
