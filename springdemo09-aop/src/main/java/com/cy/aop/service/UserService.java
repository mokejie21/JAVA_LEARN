package com.cy.aop.service;

/**
 * created by Fan
 * 2021/7/20
 */
public interface UserService {
    void addUser();

    void findUser();

    void updateUser();

    void deleteUser();
}
