package com.cy.aop.service.implement;

import com.cy.aop.service.UserService;

/**
 * created by Fan
 * 2021/7/20
 */
public class UserServiceImpl implements UserService {

    @Override
    public void addUser() {
        System.out.println("新增用户成功");
    }

    @Override
    public void findUser() {
        System.out.println("返回用户列表");
    }

    @Override
    public void updateUser() {
        System.out.println("更新用户列表");
    }

    @Override
    public void deleteUser() {
        System.out.println("删除用户成功");
    }
}
