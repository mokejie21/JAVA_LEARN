package BTree;

import javax.xml.soap.Node;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * @title: BTree
 * @Date: 2021/10/3 19:44
 * @Version 1.0
 */
public class BTree {

    private BTNode root;

    public BTree() {
    }

    public BTree(BTNode root) {
        this.root = root;
    }

    public BTNode getRoot() {
        return root;
    }

    public void setRoot(BTNode root) {
        this.root = root;
    }

    //清空子树的 所有节点
    public void clear(BTNode node) {
        if (node != null) {
            clear(node.getLeftChirld());
            clear(node.getRightChirld());
            node = null;
        }
    }

    //清空树
    public void clear() {
        clear(root);
    }

    public boolean isEmpty() {
        return root == null;
    }

    public int heigh() {
        return heigh(root);
    }

    public int heigh(BTNode node) {
        if (node == null) {
            return 0;
        } else {
            int l = heigh(node.getLeftChirld());
            int r = heigh(node.getRightChirld());

            return l > r ? (l + 1) : (r + 1);
        }
    }

    //获取树的节点数
    public int size() {
        return size(root);
    }

    public int size(BTNode node) {
        if (node == null) {
            return 0;
        } else {
            return 1 + size(node.getLeftChirld()) + size(node.getRightChirld());
        }
    }

    //获取父节点
    public BTNode getParent(BTNode node) {
        return (root == null || root == node) ? null : getParent(root, node);
    }

    public BTNode getParent(BTNode subTree, BTNode node) {
        if (subTree == null) {
            return null;
        }
        if (subTree.getLeftChirld() == node || subTree.getRightChirld() == node) {
            return subTree;
        }
        BTNode parent = null;
        if (getParent(subTree.getLeftChirld(), node) != null) {
            parent = getParent(subTree.getLeftChirld(), node);
            return parent;
        } else {
            return getParent(subTree.getRightChirld());
        }
    }

    public BTNode getLeftTree(BTNode node) {
        return node.getLeftChirld();
    }

    public BTNode getRightTree(BTNode node) {
        return node.getRightChirld();
    }

    public void inserRoot(BTNode node) {
        this.root = node;
    }

    public void inserLeft(BTNode parent, BTNode newNode) {
        parent.setLeftChirld(newNode);
    }

    public void insertRight(BTNode parent, BTNode newNode) {
        parent.setRightChirld(newNode);
    }

    public void PreOrder(BTNode node) {
        if (node != null) {
            System.out.println(node.getData());
            PreOrder(node.getLeftChirld());
            PreOrder(node.getRightChirld());
        }

    }

    public void InOrder(BTNode node) {
        if (node != null) {
            InOrder(node.getLeftChirld());
            System.out.println(node.getData());
            InOrder(node.getRightChirld());
        }
    }

    public void PostOrder(BTNode node) {
        if (node != null) {
            PostOrder(node.getLeftChirld());
            PostOrder(node.getRightChirld());
            System.out.println(node.getData());
        }
    }

    public List<Object> LevelOrder(BTNode node) {
        LinkedList<Object> list = new LinkedList<>();
        if (node == null) {
            return list;
        }
        Deque<BTNode> deque = new ArrayDeque<>();
        deque.addLast(node);
        while (!deque.isEmpty()) {
            int nums = deque.size();
            LinkedList<Object> subList = new LinkedList<>();
            for (int i = 0; i < nums; i++) {
                BTNode nodeFirst = deque.removeFirst();
                if (nodeFirst.getLeftChirld() != null) deque.addLast(nodeFirst.getLeftChirld());
                if (nodeFirst.getRightChirld() != null) deque.addLast(nodeFirst.getRightChirld());
                subList.add(nodeFirst.getData());
            }
            list.add(subList);
        }
        return list;

    }


}
