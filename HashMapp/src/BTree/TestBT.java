package BTree;

/**
 * @title: TestBT
 * @Date: 2021/10/3 20:19
 * @Version 1.0
 */
public class TestBT {
    public static void main(String[] args) {
        BTree bTree = new BTree();
        BTNode A = new BTNode("A");
        BTNode B = new BTNode("B");
        BTNode C = new BTNode("C");
        BTNode D = new BTNode("D");
        BTNode E = new BTNode("E");
        BTNode F = new BTNode("F");
        BTNode G = new BTNode("G");
        BTNode H = new BTNode("H");
        BTNode I = new BTNode("I");
        BTNode J = new BTNode("J");


        bTree.inserRoot(A);
        bTree.inserLeft(A, B);
        bTree.insertRight(A, C);
        bTree.inserLeft(B, D);
        bTree.insertRight(B, E);
        bTree.inserLeft(C, F);
        bTree.insertRight(C, G);
        bTree.inserLeft(D, H);
        bTree.insertRight(D, I);
        bTree.inserLeft(E, J);

//        bTree.PreOrder(A);
//        bTree.InOrder(A);
//        bTree.PostOrder(A);
//        System.out.println(bTree.getParent(I).getData());
//        System.out.println(bTree.size());
//        System.out.println(bTree.heigh());
        System.out.println(bTree.LevelOrder(A));
    }

}
