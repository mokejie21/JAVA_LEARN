package BTree;

/**
 * @title: BTree.BTNode
 * @Date: 2021/10/3 19:38
 * @Version 1.0
 */

public class BTNode {

    private Object data;
    private BTNode leftChirld;
    private BTNode rightChirld;

    public BTNode() {
    }

    public BTNode(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public BTNode getLeftChirld() {
        return leftChirld;
    }

    public BTNode getRightChirld() {
        return rightChirld;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void setLeftChirld(BTNode leftChirld) {
        this.leftChirld = leftChirld;
    }

    public void setRightChirld(BTNode rightChirld) {
        this.rightChirld = rightChirld;
    }
}
