/**
 * @title: Tree
 * @Date: 2021/10/3 19:22
 * @Version 1.0
 */
public class Tree {
    private static class TreeNode<E> {
        E e;
        int parent;
        //子节点
        TreeNodeChild<E> firstChild;

        public TreeNode(E e, int parent) {
            this.e = e;
            this.parent = parent;
        }
    }

    private static class TreeNodeChild<E> {
        int index;
        TreeNodeChild<E> next;

        public TreeNodeChild(int index, TreeNodeChild<E> next) {
            this.index = index;
            this.next = next;
        }
    }

}
