import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @title: TestHashMap
 * @Date: 2021/10/2 20:18
 * @Version 1.0
 */
public class TestHashMap {

    public static void main(String[] args) {
        HashMap<Object, Object> map = new HashMap<>(1000);
        ConcurrentMap<Object, Object> concurrentMap = new ConcurrentHashMap<>(1000);
        Hashtable<Object, Object> objectObjectHashtable = new Hashtable<>();
        AtomicInteger atomicInteger = new AtomicInteger();
    }
}
