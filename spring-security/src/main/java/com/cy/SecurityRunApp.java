package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

/**
 * created by Fan
 * 2021/8/6
 */
@EnableGlobalMethodSecurity(prePostEnabled = true)
@SpringBootApplication
public class SecurityRunApp {
    public static void main(String[] args) {
        SpringApplication.run(SecurityRunApp.class, args);
    }

}
