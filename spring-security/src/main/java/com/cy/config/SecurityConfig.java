package com.cy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * created by Fan
 * 2021/8/6
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        //自定义登陆表单
        http.formLogin()
                //设置登陆页面
                .loginPage("/login.html")
                //设置登陆请求处理地址(对应form表单中的action),
//登陆时会访问UserDetailService对象
                .loginProcessingUrl("/login")
                //设置请求用户名参数为username(默认就是username，
//可以自己修改，需要与表单同步)
                .usernameParameter("username")
                //请求请求密码参数为password(默认就是password，
//可以自己修改，需要与表单同步)
                .passwordParameter("password")
                //设置登陆成功跳转页面(默认为/index.html)
                .successHandler(new RedirectAuthenticationSuccessHandler("https://www.baidu.com"))
                //登陆失败访问的页面（默认为/login.html?error）
                .failureHandler(new RedirectAuthenticationFailureHandler("https://www.tmooc.cn/"));
        //认证设计
        http.authorizeRequests()
                //设置要放行的咨询
                .antMatchers("/index.html",
                        "/js/*",
                        "/css/*",
                        "/img/**",
                        "/bower_components/**",
                        "/login.html").permitAll()
                //设置需要认证的请求（除了上面的要放行，其它都要进行认证）
                .anyRequest().authenticated().and()          //上面的配置完毕,开始另一配置
                .formLogin()   //使用表单登录
                .loginPage("/login.html") //设置登录页面路径
                .loginProcessingUrl("/login") //设置处理登录的路径,SpringSecurity默认的
                .failureUrl("/login.html?error") //登录失败访问的页面
                .defaultSuccessUrl("/index.html") //登录成功访问的页面
                .and()          //上面的配置完毕,开始另一配置
                .logout()       //开始设置登出信息
                .logoutUrl("/logout")   //登出路径
                .logoutSuccessUrl("/login.html?logout");//设置登出后显示的页面

    }
}
