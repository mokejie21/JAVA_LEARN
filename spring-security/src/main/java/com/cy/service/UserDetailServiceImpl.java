package com.cy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * created by Fan
 * 2021/8/6
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        if (!"jim".equals(username)) {
            throw new UsernameNotFoundException("user not found");
        }
        //假设这个密码是从数据库查询出来的加密密码
        String password = passwordEncoder.encode("123456");
        //假设如下对象信息是从数据库查询出来的，第三个参数为用户的权限
        return new User(username, password,
                AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_admin,ROLE_normal,/doRetrieve,/doCreate"));
    }
}
