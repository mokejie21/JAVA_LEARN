package com.cy.pojo;

/**
 * created by Fan
 */
public class User {
    private String name;
    private Integer age;

    public User(String name) {
        this.name = name;
        System.out.println("您的名字是:" + name);
    }

    public User(Integer age) {
        this.age = age;
        System.out.println("您的年龄:" + age);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
