package com.cy.test;

import com.cy.dao.MySqlImpl;
import com.cy.dao.OrcalImpl;
import com.cy.service.UserService;
import com.cy.service.UserServiceImpl;

/**
 * created by Fan
 */
public class TestSpring {
    public static void main(String[] args) {
        UserService userService = new UserServiceImpl();
        userService.setUser(new OrcalImpl());
        userService.getUser();
    }
}
