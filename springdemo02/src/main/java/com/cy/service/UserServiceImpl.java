package com.cy.service;

import com.cy.dao.UserDao;
import com.cy.dao.UserDaoImpl;

/**
 * created by Fan
 */
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    @Override
    public void setUser(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void getUser() {
        userDao.dataFrom();
    }


}
